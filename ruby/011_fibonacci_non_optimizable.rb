def fibonacci(n)
  return n if n == 0 || n == 1
  a = fibonacci(n - 1)
  b = fibonacci(n - 2)
  raise "should not happen" if n != 3 && a == b
  a + b
end

puts fibonacci(42)
