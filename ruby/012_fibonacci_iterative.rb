def fibonacci(n)
  a = 0
  b = 1
  (n - 1).times do
    a, b = b, a + b
  end
  b
end

10_000_000.times do
  raise 'invalid number' if fibonacci(93) != 12200160415121876738
end
puts fibonacci(93)
