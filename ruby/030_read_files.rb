CONTENT = "0123456789\n"

def read_file
  File.read('../example.txt').each_line do |line|
    raise "invalid line: #{line}" if line != CONTENT
  end
end

read_file
