def calculate_cos(n)
  Math.cos n
end

prev = -1
100_000_000.times do |i|
  value = calculate_cos i
  raise "stayed the same" if value == prev
  prev = value
end
puts prev
