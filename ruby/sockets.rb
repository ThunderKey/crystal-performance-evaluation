require 'socket'

Socket.tcp_server_loop '127.0.0.1', 2000 do |client, client_addr|
  client.gets
  client.print(
    "HTTP/1.1 200 OK\r\n" +
    "Content-Type: text/plain\r\n" +
    "Content-Length: 12\r\n" +
    "Connection: close\r\n\r\n" +
    "Test Message"
  )
ensure
  client.close
end
