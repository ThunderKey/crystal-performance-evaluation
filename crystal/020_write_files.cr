CONTENT = "0123456789\n"

def write_file
  File.open("../tmp/crystal_file.tmp", "w") do |file|
    100_000_000.times do
      file.print CONTENT
    end
  end
end

write_file
