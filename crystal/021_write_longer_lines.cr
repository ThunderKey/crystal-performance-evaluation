CONTENT = 10.times.map { "0123456789\n" }.join

def write_file
  File.open("../tmp/crystal_file.tmp", "w") do |file|
    10_000_000.times do
      file.print CONTENT
    end
  end
end

write_file
