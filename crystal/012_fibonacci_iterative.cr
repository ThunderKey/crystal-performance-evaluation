def fibonacci(n)
  a = 0_u64
  b = 1_u64
  (n - 1).times do
    a, b = b, a + b
  end
  b
end

10_000_000.times do
  raise "invalid number" if fibonacci(93) != 12200160415121876738
end
puts fibonacci(93)
