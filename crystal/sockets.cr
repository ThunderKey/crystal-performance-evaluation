require "socket"

MESSAGE = "HTTP/1.1 200 OK\r\n" +
  "Content-Type: text/plain\r\n" +
  "Content-Length: 12\r\n" +
  "Connection: close\r\n\r\n" +
  "Test Message"

TCPServer.open 2000 do |server|
  puts "ready"
  while client = server.accept?
    client.gets
    client.print MESSAGE
    client.close
  end
end
