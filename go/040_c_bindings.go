package main

import "fmt"

/*
#cgo LDFLAGS: -lm
#include <math.h>
*/
import "C"

func calculateCos(n float64) float64 {
  val, _ := C.cos(C.double(n))
  return float64(val)
}

func main() {
  prev := float64(-1)
  for i := 0; i < 100000000; i++ {
    value := calculateCos(float64(i))
    if prev == value {
      panic("stayed the same")
    }
    prev = value
  }
  fmt.Println(prev)
}
