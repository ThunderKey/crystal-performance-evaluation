package main

import (
  "os"
  "bufio"
)

func writeFile() {
  content := "0123456789\n"

  f, err := os.Create("../tmp/go_file.tmp")
  if err != nil {
    panic(err)
  }
  defer f.Close()

  w := bufio.NewWriter(f)

  for i := 0; i < 100000000; i++ {
    w.WriteString(content)
  }
  w.Flush()
}

func main() {
  writeFile()
}
