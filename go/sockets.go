package main

import (
  "net"
  "fmt"
)

func main() {
  message := []byte("HTTP/1.1 200 OK\r\n" +
  "Content-Type: text/plain\r\n" +
  "Content-Length: 12\r\n" +
  "Connection: close\r\n\r\n" +
  "Test Message")

  listener, _ := net.Listen("tcp", ":2000")

  fmt.Println("Ready")

  for {
    conn, _ := listener.Accept()
    var buf [512]byte
    conn.Read(buf[0:])
    conn.Write(message)
    conn.Close()
  }
}
