package main

import (
  "os"
  "bufio"
)

func readFile() {
  content := "0123456789"

  f, err := os.Open("../example.txt")
  if err != nil {
    panic(err)
  }
  defer f.Close()

  scanner := bufio.NewScanner(f)
  for scanner.Scan() {
      if scanner.Text() != content {
        panic("invalid line")
      }
  }
}

func main() {
  readFile()
}

