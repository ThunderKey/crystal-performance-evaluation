package main

import (
  "fmt"
  "time"
)

func main() {
  start := time.Now()
  time.Sleep(1 * time.Second)
  elapsed := time.Since(start)
  fmt.Printf("%.10f\n", elapsed.Seconds())
}
