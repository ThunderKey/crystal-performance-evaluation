package main

import "fmt"

func fibonacci(n int) int {
  if (n == 0 || n == 1) {
    return n
  }
  a := fibonacci(n - 1)
  b := fibonacci(n - 2)
  if n != 3 && a == b {
    panic("should not happen")
  }
  return a + b
}

func main() {
  fmt.Println(fibonacci(42));
}
