package main

import "fmt"

func fibonacci(n int) uint64 {
  var a uint64 = 0;
  var b uint64 = 1;
  var c uint64;
  for i := 1; i < n; i++ {
    c = a + b;
    a = b;
    b = c;
  }

  return b;
}

func main() {
  for i := 0; i < 10000000; i++ {
    if fibonacci(93) != 12200160415121876738 {
      panic("invalid number")
    }
  }
  fmt.Println(fibonacci(93));
}
