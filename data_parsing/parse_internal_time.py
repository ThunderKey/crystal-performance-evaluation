#!/usr/bin/env python

import sys

import pandas

from helpers import RAW_OUT_PATH, CONVERTED_OUT_PATH, translate_languages


IN_DIR = RAW_OUT_PATH
OUT_DIR = CONVERTED_OUT_PATH


def parse(file_name):
    data = pandas.read_csv(file_name).drop(['run'], axis=1)
    translate_languages(data)
    data['difference'] = data['externalTime'] - data['internalTime']
    grouped = data.groupby(['language']).agg(['mean', 'min', 'max', 'std'])
    header = []
    for key in ['InternalTime', 'ExternalTime', 'Difference']:
        for agg in ['mean', 'min', 'max', 'std']:
            header.append(agg + key)
    print(grouped)
    grouped.to_csv(OUT_DIR.joinpath(f'internal_time.csv'), header=header)


parse(IN_DIR.joinpath('internal_time.csv'))
