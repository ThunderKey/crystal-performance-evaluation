#!/usr/bin/env python

import re
import subprocess
import platform

import pandas
import psutil

from helpers import CONVERTED_OUT_PATH, latex_escape


OUT_DIR = CONVERTED_OUT_PATH


def replace_all(regex, replacement):
    regex = re.compile(f'\A{regex}.*\Z', re.DOTALL)
    return regex.sub(r'\1', replacement)


def binary_prefix(num, suffix='B'):
    for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
        if abs(num) < 1024.0:
            return "%3.1f %s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f %s%s" % (num, 'Yi', suffix)


def cpu_information():
    if platform.system() == "Windows":
        return platform.processor()
    elif platform.system() == "Darwin":
        return subprocess.check_output(['/usr/sbin/sysctl', '-n', 'machdep.cpu.brand_string']).strip()
    elif platform.system() == "Linux":
        f = open('/proc/cpuinfo', 'r')
        for line in f.readlines():
            if "model name" in line:
                return re.sub( ".*model name.*:", "", line,1).strip()


def os_information():
    return platform.platform()


def ram_information():
    return binary_prefix(psutil.virtual_memory().total)


def disk_information():
    output = subprocess.check_output(['lshw', '-class', 'disk', '-class', 'storage']).decode()
    description = replace_all(r'.*\n[ ]+description: ([^\n]+)\n.*', output)
    product = replace_all(r'.*\n[ ]+product: ([^\n]+)\n.*', output)
    return f'{description} - {product}'


def time_version():
    output = subprocess.check_output(['yay', '-Qii', 'time']).decode()
    return replace_all(r'.*Version\s*: (\S+)', output)


def crystal_version():
    output = subprocess.check_output(['crystal', '--version']).decode()
    crystal = replace_all(r'Crystal\s+(\S+)', output)
    llvm = replace_all(r'.*LLVM:\s+(\S+)', output)
    return f'{crystal} (LLVM: {llvm})'


def ruby_version():
    output = subprocess.check_output(['ruby', '--version']).decode()
    return replace_all(r'ruby\s+(\S+)', output)


def gcc_version():
    output = subprocess.check_output(['gcc', '--version']).decode()
    return replace_all(r'gcc\s+\(GCC\)\s+(\S+)', output)


def go_version():
    output = subprocess.check_output(['go', 'version']).decode()
    return replace_all(r'go\s+version\s+go(\S+)', output)


def write_csv(data_dict, file_name):
    data = pandas.DataFrame(
        [[k, latex_escape(v)] for k, v in data_dict.items()],
        columns=['key', 'value']
    )
    print(data)
    data.to_csv(OUT_DIR.joinpath(file_name), index=False)


def get_system_information():
    write_csv({
        'OS': os_information(),
        'CPU': cpu_information(),
        'RAM': ram_information(),
        'Disk': disk_information(),
    }, 'system_information.csv')


def get_system_versions():
    write_csv({
        'Crystal': crystal_version(),
        'Ruby': ruby_version(),
        'GCC': gcc_version(),
        'Go': go_version(),
        'GNU time': time_version(),
    }, 'system_versions.csv')


if __name__ == '__main__':
    get_system_information()
    get_system_versions()
