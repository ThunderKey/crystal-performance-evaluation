#!/usr/bin/env python

import sys

import pandas

from helpers import RAW_OUT_PATH, CONVERTED_OUT_PATH, translate_languages


IN_DIR = RAW_OUT_PATH
OUT_DIR = CONVERTED_OUT_PATH


def parse(file_name):
    data = pandas.read_csv(file_name)
    translate_languages(data)
    grouped = data.drop(['run'], axis=1).groupby(['language']).agg(['mean', 'min', 'max', 'std'])
    header = []
    for key in ['Time', 'Rate']:
        for agg in ['mean', 'min', 'max', 'std']:
            header.append(agg + key)
    grouped.to_csv(OUT_DIR.joinpath('sockets.csv'), header=header)


parse(IN_DIR.joinpath('sockets.csv'))
