import re
from pathlib import Path


ROOT_PATH = Path(__file__).resolve().parent.parent
OUT_PATH = ROOT_PATH.joinpath('out')
RAW_OUT_PATH = OUT_PATH.joinpath('raw')
CONVERTED_OUT_PATH = OUT_PATH.joinpath('converted')

LANGUAGE_NAMES = {
    'ruby': 'Ruby',
    'ruby-jit': 'Ruby (JIT)',
    'c': 'C',
    'crystal': 'Crystal',
    'go': 'Go',
}


def translate_languages(data, language_key='language'):
    data[language_key] = data[language_key].map(LANGUAGE_NAMES)


def only_build(data):
    return data.query('Run=="build"')


def without_build(data):
    return data.query('Run!="build"')


def normalize(data):
    normalized = data.copy()
    normalized['time'] = normalized.groupby('language').transform(lambda x: (x - x.mean()) / x.std())
    return normalized


def latex_escape(data):
    return re.sub(r'([&%\$#_{}~\^\\])', r'\\\1', data)
