#!/usr/bin/env python

import re

import seaborn
from matplotlib import pyplot
import pandas
import textwrap
import numpy

from helpers import CONVERTED_OUT_PATH


seaborn.set_theme()


OUT_DIR = CONVERTED_OUT_PATH

LANGUAGES = ['Crystal', 'C', 'Go', 'Ruby', 'Ruby (JIT)']
NAME_REGEX = re.compile('_run$')

def extract_languages(name, data, *, key):
    original = data.loc[LANGUAGES[0], key]
    row = {'Measurement': name}
    for language in LANGUAGES:
        value = data.loc[language, key]
        row[language] = value
    return row


def get_column_key(measurement_name):
    if measurement_name == 'sockets':
        return 'meanTime'
    if measurement_name == 'internal_time':
        return 'meanDifference'
    return 'meanTotal'


def parse_file(file_name):
    name = NAME_REGEX.sub('', file_name.stem)
    measurement_data = pandas.read_csv(file_name, index_col='language')
    return extract_languages(name, measurement_data, key=get_column_key(name))


MEASUREMENT_TRANSLATIONS = {
    'internal_time': 'Startup Time',
    '010_fibonacci': 'Recursive Fibonacci',
    '011_fibonacci_non_optimizable': 'Recursive Fibonacci Without Optimisations',
    '012_fibonacci_iterative': 'Iterative Fibonacci',
    '020_write_files': 'Writing Lines to Files',
    '021_write_longer_lines': 'Writing Longer Lines to Files',
    '030_read_files': 'Reading Lines from Files',
    '040_c_bindings': 'C Bindings',
    'sockets': 'TCP Sockets',
}


def generate_barplot(original_data, file_name, height = 6, width = 7, *, xlabel='Time [s]'):
    data = pandas.DataFrame(columns=['Measurement', 'Language', 'Time'])
    for _i, item in original_data.iterrows():
        for language in LANGUAGES:
            if language in item:
                data = data.append({
                    'Measurement': MEASUREMENT_TRANSLATIONS[item.Measurement],
                    'Language': language,
                    'Time': item[language],
                }, ignore_index=True)
    pyplot.figure(figsize=(width, height))
    ax = seaborn.barplot(x='Time', y='Measurement', hue='Language', data=data)
    pyplot.xlabel(xlabel)
    pyplot.ylabel(None)
    measurements = data.Measurement.unique()
    ax.set_yticklabels([textwrap.fill(name, 30) for name in measurements])
    pyplot.tight_layout()
    path = OUT_DIR / 'images' / f'{file_name}.pdf'
    pyplot.savefig(path, format='pdf', bbox_inches='tight')


def parse(file_names):
    data = pandas.DataFrame([parse_file(file_name) for file_name in file_names]).sort_values('Measurement')
    generate_barplot(data, 'comparison')
    relative = data.copy()
    for language in LANGUAGES:
        relative[language] = 100 / data[LANGUAGES[0]] * data[language]
    mean = relative.mean()
    mean['Measurement'] = 'mean'
    median = relative.median()
    median['Measurement'] = 'median'
    relative = relative.append([mean, median]).reset_index(drop=True)
    print(relative)
    relative.to_csv(OUT_DIR / 'comparison.csv')

    for language in LANGUAGES:
        relative[language] /= 100
    print(relative)
    relative.to_csv(OUT_DIR / 'comparison_numeric.csv')

    relative = relative.iloc[:-2, :]
    print(relative)
    generate_barplot(relative, 'comparison_relative', height=4.5, width=10.5, xlabel='Relative Running Time to Crystal')

    # remove extreme outliers
    relative.at[(0, 1, 2), 'Ruby'] = numpy.nan
    relative.at[(0, 1, 2), 'Ruby (JIT)'] = numpy.nan
    print(relative)
    generate_barplot(relative, 'comparison_relative_2', height=4.5, width=10.5, xlabel='Relative Running Time to Crystal')

    # remove extreme outliers
    relative = relative.drop(['Ruby', 'Ruby (JIT)'], axis=1)
    print(relative)
    generate_barplot(relative, 'comparison_relative_without_ruby', height=4.5, width=10.5, xlabel='Relative Running Time to Crystal')


parse([
    *(OUT_DIR / 'generated').glob('*_run.csv'),
    OUT_DIR / 'sockets.csv',
    OUT_DIR / 'internal_time.csv',
])
