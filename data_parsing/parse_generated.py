#!/usr/bin/env python

import sys
import re

import pandas
import seaborn
from matplotlib import pyplot

from helpers import RAW_OUT_PATH, CONVERTED_OUT_PATH, translate_languages


IN_DIR = RAW_OUT_PATH.joinpath('generated')
OUT_DIR = CONVERTED_OUT_PATH.joinpath('generated')
IMG_OUT_DIR = OUT_DIR.joinpath('images')


def load_csv(name, suffix):
    data = pandas.read_csv(IN_DIR.joinpath(f'{name}_{suffix}.csv'))
    translate_languages(data)
    data['total'] = data['kernel'] + data['user']
    return data


def write_plot(name, postfix):
    path = OUT_DIR.joinpath('images').joinpath(f'{name}_{postfix}.pdf')
    pyplot.savefig(path, format='pdf')


def generate_boxplot(data):
    axis = seaborn.boxplot('language', 'total', data=data)
    pyplot.xlabel('Language')
    pyplot.ylabel('Time [s]')


def generate_comparison_plot(data):
    line_data = pandas.melt(data.drop(['real'], axis=1), ['language', 'run'])
    axis = seaborn.relplot(x='run', y='value', data=line_data, col='language', col_wrap=2, hue='variable', style='variable', markers=True, kind='line', height=3, aspect=1)
    axis.set_axis_labels('Run', 'Time [s]')
    axis.set_titles("Language = {col_name}")
    axis.legend.set_title('')


def parse_build(name):
    data = load_csv(name, 'build')
    # generate_boxplot(data)


def parse_run(name):
    data = load_csv(name, 'run')
    data_without_ruby = data.query('language!="ruby"')
    # generate_boxplot(data)
    # generate_boxplot(data_without_ruby)
    # generate_comparison_plot(data)
    # write_plot(name, 'performance_comparison')

    grouped = data.drop(['run'], axis=1).groupby(['language']).agg(['mean', 'min', 'max', 'std'])
    header = []
    for key in ['Real', 'Kernel', 'User', 'Total']:
        for agg in ['mean', 'min', 'max', 'std']:
            header.append(agg + key)
    print(grouped)
    grouped.to_csv(OUT_DIR.joinpath(f'{name}_run.csv'), header=header)


def parse(name):
    parse_build(name)
    parse_run(name)


def parse_all(args):
    if len(args) == 1:
        regex = re.compile('_build$')
        names = [regex.sub('', file_name.stem) for file_name in IN_DIR.glob('*_build.csv')]
    else:
        names = args[1:]

    for name in names:
        parse(name)


if not IMG_OUT_DIR.exists():
    IMG_OUT_DIR.mkdir(parents=True)

parse_all(sys.argv)
