#include <stdio.h>
#include <stdlib.h>

unsigned long long fibonacci(int n) {
  unsigned long long a = 0;
  unsigned long long b = 1;
  unsigned long long c;
  for (int i = 1; i < n; i++) {
    c = a + b;
    a = b;
    b = c;
  }

  return b;
}

int main() {
  for (int i = 0; i < 10000000; i++) {
    if (fibonacci(93) != 12200160415121876738ul) {
      printf("invalid number");
      exit(1);
    }
  }
  printf("%llu\n", fibonacci(93));
  return 0;
}
