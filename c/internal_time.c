#include <unistd.h>
#include <stdio.h>
#include <time.h>

#define BILLION 1e9

int main() {
  struct timespec start, end;
  clock_gettime(CLOCK_REALTIME, &start);
  sleep(1);
  clock_gettime(CLOCK_REALTIME, &end);
  double time = (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec)
            / BILLION;
  printf("%.10lf\n", time);

  return 0;
}
