#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>

char* message = "HTTP/1.1 200 OK\r\n"
"Content-Type: text/plain\r\n"
"Content-Length: 12\r\n"
"Connection: close\r\n\r\n"
"Test Message";

int main() {
  char dataSending[1025];
  char buffer[1024] = {0};
  int clintListn = 0, clintConnt = 0;
  struct sockaddr_in ipOfServer;
  clintListn = socket(AF_INET, SOCK_STREAM, 0);
  memset(&ipOfServer, '0', sizeof(ipOfServer));
  memset(dataSending, '0', sizeof(dataSending));
  ipOfServer.sin_family = AF_INET;
  ipOfServer.sin_addr.s_addr = htonl(INADDR_ANY);
  ipOfServer.sin_port = htons(2000);
  if (bind(clintListn, (struct sockaddr*)&ipOfServer , sizeof(ipOfServer))) {
    printf("bind failed\n");
    exit(1);
  }
  if (listen(clintListn , 20)) {
    printf("listen failed\n");
    exit(1);
  }

  while(1) {
    clintConnt = accept(clintListn, (struct sockaddr*)NULL, NULL);

    read(clintConnt, buffer, 1024);
    snprintf(dataSending, sizeof(dataSending), message);
    write(clintConnt, dataSending, strlen(dataSending));

    close(clintConnt);
  }

  return 0;
}
