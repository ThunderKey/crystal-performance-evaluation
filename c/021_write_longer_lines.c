#include <stdio.h>

char* content = "0123456789\n0123456789\n0123456789\n0123456789\n0123456789\n0123456789\n0123456789\n0123456789\n0123456789\n0123456789\n";

void write_file() {
  FILE *file;
  file = fopen("../tmp/c_file.tmp", "w");

  for (int i = 0; i < 10000000; i++) {
    fprintf(file, content);
  }
}

int main() {
  write_file();
  return 0;
}
