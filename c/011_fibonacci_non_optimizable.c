#include <stdio.h>
#include <stdlib.h>

int fibonacci(int n) {
  if (n == 0 || n == 1) {
    return n;
  }
  int a = fibonacci(n - 1);
  int b = fibonacci(n - 2);
  if (n != 3 && a == b) {
    printf("should not happen\n");
    exit(1);
  }
  return a + b;
}

int main() {
  printf("%d\n", fibonacci(42));
  return 0;
}
