#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double calculateCos(double n) {
  return cos((double) n);
}

int main() {
  double prev = -1;
  for (int i = 0; i < 100000000; i++) {
    double value = calculateCos(i);
    if (prev == value) {
      printf("stayed the same...");
      exit(1);
    }
    prev = value;
  }
  printf("%.16f\n", prev);
  return 0;
}
