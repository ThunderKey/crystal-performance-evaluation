#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* content = "0123456789";

void read_file() {
  FILE *file;
  file = fopen("../example.txt", "r");
  if (file == NULL) {
    printf("file not found\n");
    exit(1);
  }

  char *line = NULL;
  size_t len = 0;

  while (getline(&line, &len, file) != -1) {
    if (!strcmp(line, content)) {
      printf("invalid line %s", line);
      exit(1);
    }
  }
}

int main() {
  read_file();
  return 0;
}
