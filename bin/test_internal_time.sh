#!/bin/zsh

set -e

TEST_RUNS=10
LANGUAGES=(ruby ruby-jit c crystal go)

BIN_DIR="$(cd "$(dirname "${(%):-%N}")"; pwd)"
ROOT_DIR="$(dirname "$BIN_DIR")"

test_name="internal_time"

binary_file="out/$test_name"

declare -A build
declare -A run

build[ruby]=""
run[ruby]="ruby $test_name.rb"

build[ruby-jit]=""
run[ruby-jit]="ruby --jit $test_name.rb"

build[c]="gcc -o $binary_file -O3 -lm $test_name.c"
run[c]="./$binary_file"

build[go]="go build -o $binary_file $test_name.go"
run[go]="./$binary_file"

build[crystal]="crystal build -o $binary_file $test_name.cr --release"
run[crystal]="./$binary_file"

run_csv="$ROOT_DIR/out/raw/${test_name}.csv"

echo "language,run,internalTime,externalTime" > $run_csv

echo Build
for lang in ${LANGUAGES[@]}; do
  [[ "$lang" == "ruby-jit" ]] && real_lang="ruby" || real_lang="$lang"
  echo "# $lang #"
  pushd $ROOT_DIR/$real_lang > /dev/null
  [[ -f "$binary_file" ]] && rm "$binary_file"
  eval ${build[$lang]}
  popd > /dev/null
done

for i in $(seq $TEST_RUNS); do
  echo Run $i
  for lang in ${LANGUAGES[@]}; do
    [[ "$lang" == "ruby-jit" ]] && real_lang="ruby" || real_lang="$lang"
    echo "# $lang #"
    pushd $ROOT_DIR/$real_lang > /dev/null

    unset internal_time real_time
    eval "$( eval time ${run[$lang]} \
      2> >(real_time=$(cat | sed -r 's/^.* ([0-9][0-9.]*) total$/\1/'); typeset -p real_time) \
      > >(internal_time=$(cat); typeset -p internal_time) )"
    echo "$lang,$i,$internal_time,$real_time" >> $run_csv
    popd > /dev/null
  done
done
