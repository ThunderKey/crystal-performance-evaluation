#!/bin/bash

set -e

TEST_RUNS=10
LANGUAGES="ruby ruby-jit crystal go c"

BIN_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")"; pwd)"
ROOT_DIR="$(dirname "$BIN_DIR")"

RUN_CSV="$ROOT_DIR/out/raw/sockets.csv"
echo "language,run,time,rate" > $RUN_CSV

URL=http://127.0.0.1:2000/

for lang in $LANGUAGES; do
  echo "Please start $lang"
  read tmp

  if [[ "$(curl $URL 2> /dev/null)" != "Test Message" ]]; then
    echo failed
    exit 1
  fi

  for i in $(seq $TEST_RUNS); do
    echo "Run $i"
    output="$(siege $URL --concurrent=1 --reps=10000 --benchmark 2>&1)"
    rate="$(echo "$output" | grep "Transaction rate:" | sed -E 's/^[^0-9]*([0-9][0-9.]*) trans\/sec$/\1/')"
    time="$(echo "$output" | grep "Elapsed time:" | sed -E 's/^[^0-9]*([0-9][0-9.]*) secs$/\1/')"
    echo "$lang,$i,$time,$rate" >> $RUN_CSV
  done
done
