#!/bin/bash

set -e

TEST_RUNS=10
LANGUAGES="ruby ruby-jit c crystal go"

BIN_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")"; pwd)"
ROOT_DIR="$(dirname "$BIN_DIR")"

test_name="$1"

if [[ -z "$test_name" ]]; then
  echo No test name given.
  echo Usage: ${BASH_SOURCE[0]} TEST_NAME
  exit 1
fi

binary_file="out/$test_name"

declare -A build
declare -A run

build[ruby]=""
run[ruby]="ruby $test_name.rb"

build[ruby-jit]=""
run[ruby-jit]="ruby --jit $test_name.rb"

build[c]="gcc -o $binary_file -O3 -lm $test_name.c"
run[c]="./$binary_file"

build[go]="go build -o $binary_file $test_name.go"
run[go]="./$binary_file"

build[crystal]="crystal build -o $binary_file $test_name.cr --release"
run[crystal]="./$binary_file"

time_command() {
  language=$1
  shift
  run_id=$1
  shift
  run_id_csv=",$run_id"
  [[ $run_id == "build" ]] && run_id_csv=""
  if ! [[ -z "$@" ]]; then
    # use the GNU time command explicitly
    /usr/bin/time -f "$language$run_id_csv,%e,%S,%U" "$@"
  fi
}

build_csv="$ROOT_DIR/out/raw/generated/${test_name}_build.csv"
run_csv="$ROOT_DIR/out/raw/generated/${test_name}_run.csv"

echo "language,real,kernel,user" > $build_csv
echo "language,run,real,kernel,user" > $run_csv

echo Build
for lang in $LANGUAGES; do
  [[ "$lang" == "ruby-jit" ]] && real_lang="ruby" || real_lang="$lang"
  echo "# $lang #"
  pushd $ROOT_DIR/$real_lang > /dev/null
  [[ -f "$binary_file" ]] && rm "$binary_file"
  time_command $lang build ${build[$lang]} 2>> $build_csv
  popd > /dev/null
done

for i in $(seq $TEST_RUNS); do
  echo Run $i
  for lang in $LANGUAGES; do
    [[ "$lang" == "ruby-jit" ]] && real_lang="ruby" || real_lang="$lang"
    echo "# $lang #"
    pushd $ROOT_DIR/$real_lang > /dev/null
    time_command $lang $i ${run[$lang]} 2>> $run_csv
    temp_file=../tmp/${real_lang}_file.tmp
    if [[ -f $temp_file ]]; then
      du -sh $temp_file
      rm $temp_file
    fi
    popd > /dev/null
  done
done
